#include <iostream>
#include <fstream>

enum Coins {
  One = 1,
  Three = 3,
  Five = 5
};

int main(const int argc, const char *argv[]) {

  std::ifstream filestream(argv[1]);
  int amount;

  while ( filestream >> amount ) {

    unsigned int num_of_coins{0};
    while ( amount ) {
      if ( amount - Coins::Five >= 0 ) {
        amount -= Coins::Five;
        num_of_coins++;
      } else if ( amount - Coins::Three >= 0 ) {
        amount -= Coins::Three;
        num_of_coins++;
      } else {
        amount -= Coins::One;
        num_of_coins++;
      }
    }

    std::cout << num_of_coins << "\n";
  }

  return 0;
}
