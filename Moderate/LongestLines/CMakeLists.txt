cmake_minimum_required(VERSION 3.1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES longest_lines.cpp)
add_executable(longest_lines ${SOURCE_FILES} longest_lines.cpp)
set_target_properties(longest_lines PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin"
)
