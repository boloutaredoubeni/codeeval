#include <iostream>
#include <fstream>
#include <map>

int main(const int argc, const char *argv[]) {
  std::ifstream file_stream( argv[1] );
  unsigned int N;
  // Read the first input as a number
  file_stream >> N;

  std::map<unsigned int,std::string> m;
  std::string sentance;
  while ( std::getline( file_stream, sentance) ) {
    m.insert( std::pair<unsigned int,std::string>(sentance.size(), sentance ));
  }

  // Print out the N longest numbers
  std::map<unsigned int, std::string>::const_reverse_iterator it{ m.rbegin() };
  while ( N-- ) {
    std::cout << it->second << "\n";
    ++it;
  }

  return 0;
}
