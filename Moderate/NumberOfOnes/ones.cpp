#include <iostream>
#include <fstream>

int main(const int argc, const char *argv[]) {

  std::ifstream filestream(argv[1]);
  unsigned int b;
  while ( filestream >> b ) {
    int count{0};
    while (b) {
      if ( (b%2) == 1 ) {
        ++count;
      }
      b >>= 1;
    }

    std::cout << count << "\n";
  }

  return 0;
}
