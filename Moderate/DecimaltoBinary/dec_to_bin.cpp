#include <iostream>
#include <fstream>
#include <string>

void to_binary(int);

int main(int argc, const char *argv[]){
	std::ifstream file(argv[1]);
	std::string line;

	while(std::getline(file, line)){
		int b = std::stoi(line);
		to_binary(b);
		std::cout << "\n";
	}
	return(0);
}

void to_binary(int num){
	if (num < 2){
		std::cout << num;
		return;
	}

	int r = num % 2;
	to_binary(num >> 1);
	std::cout << r;
}
