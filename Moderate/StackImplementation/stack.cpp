#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

int main(const int argc, const char *argv[]) {

  std::ifstream filestream(argv[1]);
  std::string line;
  while ( std::getline( filestream, line )) {
    std::stringstream ss(line);
    int n;
    std::vector<int> v;
    while ( ss >> n ) {
      v.push_back(n);
    }

    bool should_pop = true;
    for ( auto it = v.rbegin(); it != v.rend(); ++it ) {
      if ( should_pop ) {
        std::cout << *it << " ";
      }
      should_pop = !should_pop;
    }

    std::cout << "\n";
  }

  return 0;
}
