#include <iostream>
#include <fstream>
#include <string>
#include <set>


// FIXME: only 72.5%
int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;
  while ( filestream >> line ) {
    if ( line.empty() ) {
      continue;
    }
    if ( line.size() == 1 ) {
      std::cout << line << "\n";
      continue;
    }

    int count{0};
    std::set<char> repeats;
    for(std::string::const_iterator it = line.begin(); it != line.end(); ++it){
      if (repeats.find(*it) != repeats.end()) {
        continue;
      }
      if ( it == line.end()-1 ) {
        std::cout << *it << "\n";
        break;
      }
      auto index_pos = line.find_first_of(*it, count+1);
      if( index_pos == std::string::npos ){
        std::cout << *it << "\n";
        break;
      } else {
        repeats.insert(line.at(count+1));
      }
      ++count;
    }
  }

  return 0;
}
