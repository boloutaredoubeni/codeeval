#include <iostream> // std::cout
#include <fstream> // std::ifstream
#include <sstream> // std::istringstream
#include <set> // std::set, TODO: change to std::vector
#include <utility> // std::pair
#include <memory> // std::unique_ptr

// FIXME: only 75% correct
// A class to hold vertices to build the Rectangles
class Rectangle {
public:
  Rectangle(int x1,int y1,int x2,int y2): x1_(x1), y1_(y1), x2_(x2), y2_(y2) {}

  bool doesOverlapWith(std::shared_ptr<Rectangle> &other);


private:
  int x1_, y1_, x2_, y2_;
  // Methods to obtain the vertices
  inline std::pair<int,int> getUpperLeft();
  inline std::pair<int,int> getUpperRight();
  inline std::pair<int,int> getLowerRight();
  inline std::pair<int,int> getLowerLeft();
  std::set<std::pair<int,int>> getVertices();
};

inline std::pair<int,int> Rectangle::getUpperLeft() {
  return std::pair<int,int> ( this->x1_, this->y1_ );
}

inline std::pair<int,int> Rectangle::getUpperRight() {
  return std::pair<int,int> ( this->x2_, this->y1_ );
}

inline std::pair<int,int> Rectangle::getLowerLeft() {
  return std::pair<int,int> ( this->x1_, this->y2_ );
}

inline std::pair<int,int> Rectangle::getLowerRight() {
  return std::pair<int,int> ( this->x2_, this->y2_ );
}

// Collect the vertices that belong to the Rectangle
std::set<std::pair<int,int>> Rectangle::getVertices() {
  std::set<std::pair<int,int>> vertices;
  vertices.insert( this->getUpperLeft() );
  vertices.insert( this->getUpperRight() );
  vertices.insert( this->getLowerLeft() );
  vertices.insert( this->getLowerRight() );
  return vertices;
}


bool Rectangle::doesOverlapWith(std::shared_ptr<Rectangle> &other) {
  // collect all possible points
  auto allVertices = other->getVertices();
  // loop and check
  auto it = allVertices.begin();
  while ( it != allVertices.end() ) {
    if ((it->first >= this->x1_) && (it->first <= this->x2_) &&
        (it->second >= this->y2_) && (it->second <= this->y1_)) {
      return true;
    }
    ++it;
  }
  return false;
}


int main(const int argc, const char* argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline( filestream, line ) ) {
    std::istringstream ss(line);

    int ax1, ay1, ax2, ay2;
    int bx1, by1, bx2, by2;

    // TODO: cleanup
    ss >> ax1;
    if(ss.peek() == ',')  {
      ss.ignore();
    }
    ss >> ay1;
    if(ss.peek() == ',')  {
      ss.ignore();
    }
    ss >> ax2;
    if(ss.peek() == ',')  {
      ss.ignore();
    }
    ss >> ay2;
    if(ss.peek() == ',')  {
      ss.ignore();
    }
    ss >> bx1;
    if(ss.peek() == ',')  {
      ss.ignore();
    }
    ss >> by1;
    if(ss.peek() == ',')  {
      ss.ignore();
    }
    ss >> bx2;
    if(ss.peek() == ',')  {
      ss.ignore();
    }
    ss >> by2;

    std::unique_ptr<Rectangle> A(new Rectangle(ax1, ay1, ax2, ay2) );
    std::shared_ptr<Rectangle> B(new Rectangle(bx1, by1, bx2, by2) );

    std::cout << ( A->doesOverlapWith(B) ? "True" : "False" ) << "\n";
  }

  return 0;
}
