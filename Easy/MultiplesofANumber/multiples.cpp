#include <iostream>
#include <fstream>
#include <sstream>

int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( filestream >> line ) {
    std::istringstream ss(line);
    unsigned int x, n;

    ss >> x;
    ss.ignore(1);
    ss >> n;

    unsigned int result{n};
    while ( result < x ) {
      result += n;
    }

    std::cout << result << "\n";
  }

  return 0;
}
