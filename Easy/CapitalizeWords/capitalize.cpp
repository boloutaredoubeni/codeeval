#include <iostream>
#include <fstream>
#include <cctype>
#include <sstream>


int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline(filestream, line) ) {
    std::istringstream ss(line);
    std::string word;

    while ( ss >> word ) {
      std::string print_str("");

      for ( auto it = word.begin(); it != word.end(); ++it ) {
        if ( it == word.begin() ) {
          print_str += std::toupper(*it);
        } else {
          print_str += *it;
        }
      }

      std::cout << print_str << " ";
    }

    std::cout << "\n";
  }

  return 0;
}
