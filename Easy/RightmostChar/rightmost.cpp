#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>

int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline(filestream, line) ) {
    std::istringstream ss(line);
    std::string phrase, ch;

    std::getline(ss, phrase, ',');
    std::getline(ss, ch);

    int count{ (int)phrase.size() - 1 };
    for ( auto rt = phrase.rbegin(); ; ++rt) {
      if ( rt == phrase.rend() ) {
        std::cout << -1 << std::endl;
        break;
      } else if ( *rt == ch.c_str()[0] ) {
        std::cout << count << std::endl;
        break;
      }
      --count;
    }

  }

  return 0;
}
