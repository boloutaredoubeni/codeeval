#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

int main( const int argc, const char *argv[] ) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline(filestream, line) ) {
    std::istringstream ss(line);
    std::string word;
    std::vector<std::string> words;

    while ( ss >> word ) {
      words.push_back(word);
    }

    if ( words.size() == 1 ) {
      std::cout << words[0] << std::endl;
      break;
    }

    std::cout << words[words.size() - 2] << std::endl;

  }
  return 0;
}
