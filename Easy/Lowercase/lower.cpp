#include <iostream>
#include <fstream>
#include <algorithm>


int main(const int argc, const char *argv[]) {
  // Open a file for I/O
  std::ifstream file_stream(argv[1]);
  std::string line;

  // pipe file contents by line
  while(std::getline(file_stream,line)) {
    std::transform(line.begin(), line.end(), line.begin(), ::tolower);
    line += "\n";
    for (const auto &letter : line) std::cout << letter;
  }

  return 0;
}
