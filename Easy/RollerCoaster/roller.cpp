#include <iostream>
#include <fstream>
#include <cctype>

int main(const int argc, const char *argv[]) {

  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline(filestream, line) ) {
    std::string coaster_str("");
    bool case_flag{false};

    for (const char &ch : line) {
      if ( std::isalpha(ch) ) {
        coaster_str += (case_flag ? std::tolower(ch) : std::toupper(ch));
        case_flag = !case_flag;
      } else {
        coaster_str += ch;
      }
    }

    std::cout << coaster_str << "\n";
  }

  return 0;
}
