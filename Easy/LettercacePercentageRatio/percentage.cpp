#include <iostream>
#include <fstream>

int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;
  while(filestream >> line) {
    // NOTE: the return type of std::string::size() is std::size_t
    double len = line.size();
    double tally{0};
    for (const char &ch : line) {
      if (std::isupper(ch)) {
        ++tally;
      }
    }

    double upper_percent{(tally/len)*100};
    printf("lowercase: %.2f uppercase: %.2f\n", 100-upper_percent, upper_percent);
  }
}
