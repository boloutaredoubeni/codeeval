#include <iostream>
#include <fstream>

int main(const int argc, const char *argv[]) {
  std::ifstream file_stream(argv[1]);
  std::string line;

  while(std::getline(file_stream, line)) {
    for ( auto ch : line) {
      if (ch >= 65 && ch <= 90) std::cout << (char)(ch+32);
      else if (ch >= 97 && ch <= 122) std::cout << (char)(ch-32);
      else std::cout << ch;
    }
    std::cout << "\n";
  }

  return 0;
}
