cmake_minimum_required(VERSION 3.1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES simple_sort.cpp)
add_executable(simple_sort ${SOURCE_FILES} simple_sort.cpp)
set_target_properties(simple_sort PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin"
)
