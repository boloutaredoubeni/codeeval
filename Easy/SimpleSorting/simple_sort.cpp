#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iomanip> // std::setprecision, std::fixed

int main(const int argc, const char* argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline( filestream, line) ) {
    if ( line.empty() ) continue;
    std::istringstream ss(line);
    std::vector<float> xs;
    float x;

    while ( ss >> x ) xs.emplace_back( x );

    std::sort( xs.begin(), xs.end(), [](float a, float b) {
      return a < b;
    });

    std::cout << std::fixed << std::setprecision(3);
    std::copy(xs.begin(),xs.end(),std::ostream_iterator<float>(std::cout, " "));
    std::cout << std::endl;
  }

  return 0;
}
