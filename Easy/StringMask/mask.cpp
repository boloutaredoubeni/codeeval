#include <iostream>
#include <fstream>
#include <sstream>
#include <cctype>


int main(const int argc, const char *argv[]) {

  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline(filestream, line) ) {
    std::istringstream ss(line);
    std::string word, masked_word, b;
    ss >> word >> b;

    for ( auto it = b.begin(), jt = word.begin(); it != b.end(); ++it, ++jt ) {
      if ( *it == '0' ) {
        masked_word += *jt;
      } else {
        masked_word += std::toupper(*jt);
      }
    }

    std::cout << masked_word << std::endl;
  }

  return 0;
}
