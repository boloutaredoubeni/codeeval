#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>

// FIXME: This solution gets 95% correct
bool is_same_bitflag(unsigned int &n,
                    const unsigned int &p1,
                    const unsigned int &p2);

int main(const int argc, const char *argv[]) {
  std::ifstream file_stream(argv[1]);
  unsigned int n, p1, p2;
  std::string csv_line;

  // read each line into a string
  while(file_stream >> csv_line){
    unsigned int tmp;
    std::istringstream ss(csv_line);
    unsigned int i{0};
    // read into n, p1, p2 while ignoring commas
    while(ss >> tmp){
      if (i == 0) {
        n = tmp;
      } else if (i == 1) {
        p1 = tmp;
      } else if (i == 2) {
        p2 = tmp;
      }

      ++i;

      if(ss.peek() == ',')  {
        ss.ignore();
      }
    }

    const bool ans{is_same_bitflag(n, p1, p2)};
    std::cout << std::boolalpha << ans << '\n';
  }

  return 0;
}

bool is_same_bitflag(unsigned int &n,
                    const unsigned int &p1,
                    const unsigned int &p2) {

  auto result = std::minmax({p1, p2});
  int current_position{1}, val_at_p1, val_at_p2;
  while (current_position <= result.second || n != 0) {
    if (current_position == p1) {
      val_at_p1 = (n % 2);
    } else if (current_position == p2) {
      val_at_p2 = (n % 2);
    }
    current_position++;
    n >>= 1;
  }

  return val_at_p1 == val_at_p2;
}
