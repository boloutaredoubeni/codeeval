#include <iostream>
#include <fstream>
#include <cmath>

int main(const int argc, const char *argv[]) {

  std::ifstream file_stream(argv[1]);
  int nth;
  while (file_stream >> nth) {
    unsigned int fibs = [&nth]() -> unsigned int {
      return (1/sqrt(5)) * (pow((1 + sqrt(5)) / 2, nth) -
        pow((1 - sqrt(5)) / 2, nth));
    }();
    std::cout << fibs << "\n";
  }
}
