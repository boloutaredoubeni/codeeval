#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include <utility>


static const std::map<unsigned int, std::string> Roman {
  {1000, "M"},
  {900, "CM"},
  {500, "D"},
  {400, "CD"},
  {100, "C"},
  {90, "XC"},
  {50, "L"},
  {40, "XL"},
  {10, "X"},
  {9, "IX"},
  {5, "V"},
  {4, "IV"},
  {1, "I"}
};


int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  unsigned int numeral;

  while( filestream >> numeral ) {
    using MapIter = std::map<unsigned int, std::string>::const_reverse_iterator;

    std::string roman_numeral("");
    // NOTE: maps by default are ordered 
    for (  MapIter it = Roman.rbegin(); it != Roman.rend(); ++it ) {
      while ( numeral >= (it->first) ) {
        roman_numeral += (it->second);
        numeral -= (it->first);
      }
    }

    std::cout << roman_numeral << std::endl;
  }
}
