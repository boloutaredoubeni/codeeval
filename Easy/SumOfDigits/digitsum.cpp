#include <iostream>
#include <fstream>

int main(const int argc, const char *argv[]) {
  std::ifstream file_stream(argv[1]);
  unsigned int n;

  while (file_stream >> n) {
    unsigned int sum{0};

    while (n != 0) {
      int m(n%10);
      sum += m;
      n /= 10;
    }

    std::cout << sum << "\n";
  }
}
