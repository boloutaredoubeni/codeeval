#include <iostream>
#include <fstream>

int main(const int argc, const char *argv[]) {

  std::ifstream filestream(argv[1]);
  int n;
  while ( filestream >> n ) {
    std::cout << ((n % 2) == 0 ? 1 : 0) << "\n";
  }

  return 0;
}
