#include <iostream>
#include <cmath>


bool is_prime(const unsigned long &p);

int main() {
  unsigned long p{11}, sum{17};
  for (int n = 0; n != 996;) {
		if (is_prime(p)){
			n++;
			sum += p;
		}
		p += 2;
	}
  std::cout << sum;
  return 0;
}

bool is_prime(const unsigned long &p) {
  if ((p==1) || ((p%2)==0)) return false;
  else if ((p < 4) || (p < 9)) return true;
  else if ((p%3)==0) return false;
  const unsigned long r = std::floor(std::sqrt(p));
  unsigned long f = 5;
  while (f <= r){
      if ((p%f)==0) return false;
      else if (p%(f+2)==0) return false;
      f += 6;
  }
  return true;
}
