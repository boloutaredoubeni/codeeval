#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <utility>

// FIXME: Not Complete
using VectorString = std::vector<std::string>;
using PositionVector = std::vector<unsigned int>;
using CodePair = std::pair<VectorString, PositionVector>;


CodePair tokenize(const std::string &word_str, const std::string &pos_str);

/**
 * A helper function that return a vector ofthe specified type
 * @param io      string stream to read the str input
 * @param str     a string to read to tokenize into the vector
 * @return        return a vector with the tokenized elements
 */
template <typename T>
std::vector<T> make_vector(std::istringstream &io, const std::string &str);

/**
 * decodes a given set of tokens and returns the sentance in order
 * @param  words     token of words to be decoded
 * @param  positions the given positions of words in the original
 *                    uncoded sentance
 * @return           a string with the words in order, as a side
 *                     effect @param positions is sorted
 */
std::string decode(const VectorString &words, PositionVector &positions);

int main(const int argc, const char *argv[]) {

  std::ifstream filestream(argv[1]);

  std::string line;
  while(  std::getline(filestream, line) ) {
    std::istringstream ss(line);
    std::vector<std::string> token_line;

    std::string tmp;
    while (std::getline(ss, tmp, ';') ) {
      token_line.push_back(tmp);
    }

    CodePair pr = tokenize(token_line[0], token_line[1]);
    std::string decoded_sentance = decode(pr.first, pr.second);
    std::cout << decoded_sentance << "\n";

  }

  return 0;
}

std::string decode(const VectorString &words, PositionVector &positions) {

  // FIXME: Insert the missing number
  std::sort(positions.begin(), positions.end());
  unsigned int x{1};

  std::string decoded_string("");
  for (const unsigned int &i : positions) {
    decoded_string += (words[i-1] + " ");
  }

  // TODO: strip trailing whitespace
  return decoded_string;
}

template <typename T>
std::vector<T> make_vector(std::istringstream &io, const std::string &str) {
  std::vector<T> v;
  // FIXME: ss need input
  T elem;
  while(io >> elem) {
    v.push_back(elem);
    if (io.peek() == ' ') {
      io.ignore();
    }
  }

  return v;
}

CodePair tokenize(const std::string &word_str, const std::string &pos_str) {
  std::istringstream ss;
  VectorString words{make_vector<std::string>(ss, word_str)};
  PositionVector positions{make_vector<unsigned int>(ss, pos_str)};
  return CodePair{words, positions};
}

/**
 * __author__ = 'boloutaredoubeni'

import sys

if __name__ == "__main__":
    with open(sys.argv[1], 'r') as test_cases:
        for test in test_cases:
            # Split words by `;`
            sentance, numbers = test.split(';')
            # Split words by empty space
            words, keys = sentance.split(" "), map(int, numbers.split(" "))
            # decrement all keys by one
            keys = map(lambda x: x-1, keys)
            # Find the mising key via set operations
            missing_key = set(range(len(words))) - set(keys)
            # add the missing key
            keys.append(missing_key.pop())
            # create a lookup table to output data
            lookup_table = dict(zip(keys, words))
            # Print out the code in order
            print ' '.join(lookup_table[i] for i in sorted(keys))
 */
