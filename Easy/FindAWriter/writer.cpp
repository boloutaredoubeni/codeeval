#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline(filestream, line) ) {
    std::istringstream ss(line);
    std::string encrypted_string;

    std::getline(ss, encrypted_string, '|');

    unsigned int x;
    std::vector<unsigned int> v;

    while( ss >> x) {
      v.push_back(x);
    }

    std::string famous_writer("");
    for (const unsigned int &i : v) {
      famous_writer += encrypted_string[i-1];
    }

    std::cout << famous_writer << std::endl;

  }

  return 0;
}
