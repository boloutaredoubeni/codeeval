#include <iostream>
#include <fstream>
#include <sstream>
#include <stack>

int main(const int argc, const char* argv[]){
  std::string line;
  std::ifstream file_stream(argv[1]);
  // while there are lines
  while (std::getline(file_stream, line)) {
    std::stack<std::string> word_stack;
    // if empty skip
    if (line.empty()) continue;
    // tokenize words
    std::string word;
    std::istringstream str_stream(line);
    // push words to stack
    while(str_stream >> word)
      word_stack.push(word);

    //  then pop out to STDOUT
    while(!word_stack.empty()) {
      std::cout << word_stack.top() << ' ';
      word_stack.pop();
    }
    std::cout << '\n';
  }

  return 0;
}
