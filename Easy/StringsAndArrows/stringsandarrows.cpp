#include <iostream>
#include <fstream>
#include <string>

#define ARROW_LENGTH 5

const static std::string right_arrow("<--<<");
const static std::string left_arrow(">>-->");


int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( filestream >> line ) {
    std::size_t line_length{line.size()} , str_position{0};
    unsigned int arrow_count{0};

    while ( str_position < (line_length - ARROW_LENGTH + 1) ) {
      if ( line.substr(str_position, ARROW_LENGTH) == right_arrow ||
           line.substr(str_position, ARROW_LENGTH) == left_arrow ) {
        arrow_count++;
        str_position += (ARROW_LENGTH - 1);
      } else{
        str_position++;
      }
    }

    std::cout << arrow_count << "\n";
  }

  return 0;
}
