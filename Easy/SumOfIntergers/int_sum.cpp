#include <iostream>
#include <fstream>

int main(const int argc, const char *argv[]) {

  std::ifstream file_stream(argv[1]);
  unsigned int n, total{0};
  while(file_stream >> n) total += n;
  std:: cout << total;
  return 0;
}
