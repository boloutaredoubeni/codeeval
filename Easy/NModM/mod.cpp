#include <iostream>
#include <fstream>
#include <sstream>

int main(const int arg, const char *argv[]) {

  std::ifstream filestream(argv[1]);
  std::string line;

  while ( filestream >> line ) {
    std::istringstream ss(line);
    int n, m;
    ss >> n;
    if (ss.peek() == ',') {
      ss.ignore();
    }
    ss >> m;
    ss.clear();

    for( ; m <= n; n -= m);

    std::cout << n << "\n";
  }

  return 0;
}
