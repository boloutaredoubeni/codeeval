#include <stdio.h>

#define WIDTH 4
#define SIZE 12

/* Print out the grade school multiplication table up to 12*12. */
int main(){
  for (int i=0; i<SIZE; ++i){
    for(int j =0; j < SIZE; ++j){
      int x = (i+1)*(j+1);
      printf("%*d",WIDTH, x);
    }

    if (i < SIZE-1) puts("\n");
  }
  return 0;
}
