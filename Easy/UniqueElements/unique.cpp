#include <iostream>
#include <fstream>
#include <sstream>
#include <climits>
#include <vector>

int main(const int argc, const char *argv[]) {
  std::ifstream file_stream(argv[1]);
  std::string line;

  while(file_stream >> line) {
    std::istringstream ss{line};
    int current_number;
    int prev_number{INT_MIN};
    std::vector<int> v;

    while(ss >> current_number) {
      if (ss.peek() == ',')  ss.ignore();
      if (current_number == prev_number)
        continue;
      prev_number = current_number;
      v.push_back(current_number);
    }

    while (!v.empty()) {
      std::cout << v.front();
      if (v.size() != 1) std::cout << ",";
      v.erase(v.begin());
    }
    std::cout << "\n";
  }
  return 0;
}
