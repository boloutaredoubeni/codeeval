cmake_minimum_required(VERSION 3.1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES fizzbuzz.cpp)
add_executable(fizzbuzz ${SOURCE_FILES} fizzbuzz.cpp)
set_target_properties(fizzbuzz PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin"
)
