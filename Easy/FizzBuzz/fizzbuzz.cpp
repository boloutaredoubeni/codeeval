// fizzbuzz.cpp
#include <iostream>
#include <fstream>


int main(const int argc, const char *argv[]) {
  unsigned int x, y, n;
  // read from the file
  std::ifstream file_stream(argv[1]);
  // loop while there is still data in the file
  while (file_stream >> x >> y >> n) {
    // check every number
    for ( unsigned int i = 1; i <= n; ++i) {
      if (((i % x) == 0) && ((i % y) == 0)) std::cout << "FB";
      else if ((i % x) == 0) std::cout << "F";
      else if ((i % y) == 0) std::cout << "B";
      else std::cout << i;
      if (i < n) std::cout << " ";
    }
    // print a new line
    std::cout << "\n";
  }

  return 0;
}
