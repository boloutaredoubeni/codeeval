#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <algorithm>

int main(const int argc, const char *argv[]) {

  std::ifstream filestream(argv[1]);
  std::string line;

  while( std::getline(filestream, line) ) {
    std::vector<std::string> words;
    std::istringstream ss(line);
    std::string word;

    while( ss >> word ) {
      words.push_back(word);
    }

    const auto compare_string = [](std::string &str1, std::string &str2) -> bool {
      return str1.size() < str2.size();
    };
    const auto max_string = std::max_element(words.begin(),
                                             words.end(),
                                             compare_string);
    const auto max_idx = std::distance(words.begin(), max_string);
    const std::string max_word = words.at(max_idx);

    unsigned int i{0};
    for (const char &ch : max_word) {
      std::cout << std::string(i++, '*') << ch << " ";
    }

    std::cout << "\n";
  }

  return 0;
}
