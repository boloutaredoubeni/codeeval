#include <iostream>
#include <fstream>
#include <set>

// FIXME: Not correct
int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline(filestream, line) ) {
    unsigned int max_beauty{0};
    std::set<char> letters;
    for ( const char &ch : line ) {
      if  ( (ch >= 'a') && (ch <= 'z') ) {
        max_beauty += ( ch - 'a' + 1);
      } else if ( (ch >= 'A') &&  (ch <= 'Z') ) {
        max_beauty += ( ch - 'A' + 1);
      }
    }

    std::cout << max_beauty << std::endl;
  }
}
