#include <iostream>
#include <fstream>

int main(int argc, char *argv[]){

    std::ifstream stream(argv[1]);

    int n;
    while(stream >> std::hex >> n){
      std::cout << std::dec << n << std::endl;
    }
    return 0;
}
