// primedrome.cpp
#include <iostream>
#include <cmath>

bool is_prime(const unsigned int p);
bool is_palindrome(const unsigned int x);


int main() {
  unsigned int n{999};
  while (n > 2) {
    if (is_prime(n) && is_palindrome(n)){
      std::cout << n;
      break;
    }
    n -= 2;
  }
  return 0;
}


bool is_prime(const unsigned int &p) {
  if ((p==1) || ((p%2)==0)) return false;
    else if ((p < 4) || (p < 9)) return true;
    else if ((p%3)==0) return false;
    const unsigned int r = std::floor(std::sqrt(p));
    unsigned int f = 5;
    while (f <= r){
        if ((p%f)==0) return false;
        else if (p%(f+2)==0) return false;
        f += 6;
    }
    return true;
}

bool is_palindrome(const unsigned int &p) {
  const std::string str{std::to_string(p)};
  return str == std::string(str.rbegin(), str.rend());
}
