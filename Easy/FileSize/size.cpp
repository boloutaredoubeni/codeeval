#include <iostream>
#include <fstream>

std::ifstream::pos_type filesize(const char *filename) {
  std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
  return in.tellg();
}

int main(const int argc, const char *argv[]) {
  std::cout << filesize(argv[1]);
}
