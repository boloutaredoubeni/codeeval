#include <stdio.h>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>
#include <iostream>

int main( const int argc, const char* argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;


  while ( std::getline( filestream, line ) ) {
    if ( line.empty() ) continue;
    std::istringstream ss(line);
    std::cout << line;
    int x1, x2, y1, y2;
    ss >> x1 >> y1 >>  x2 >> y2;
    std::cout << x1 << y1 << x2 << y2;
    const int ans = [&](){
      return static_cast<int>( sqrt( pow(x2-x1, 2) + pow(y2-y1, 2) ));
    }();

    std::cout << ans << std::endl;

   }
}
