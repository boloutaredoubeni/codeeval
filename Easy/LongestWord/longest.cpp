#include <iostream>
#include <fstream>
#include <sstream>

int main(const int argc, const char *argv[]) {
  std::ifstream filestream(argv[1]);
  std::string line;

  while ( std::getline(filestream, line) ) {
    std::istringstream ss(line);
    std::string word, longestword("");

    while ( ss >> word ) {
      if ( longestword.size() < word.size() ) {
        longestword = word;
      }
    }

    std::cout << longestword << std::endl;
  }

  return 0;
}
